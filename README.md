## Run the app

npm install

npm start

Runs on localhost:3000

##Contains: 

* a working example of a sorting table functionality.
* ES6 - 7 Support with Babel.
* eslint to keep your js readable.
* Using semantic-ui css.
* Using react.js to render the frontend.
* Using moment and lodash libs.

##TODO (if more time): 

* Split components in different files
* Add redux and extend the application
* Add unit tests
