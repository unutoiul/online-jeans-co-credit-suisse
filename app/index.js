import 'babel-polyfill';

//CSS imports
import '../node_modules/semantic-ui/dist/components/button.css';
import '../node_modules/semantic-ui/dist/components/table.css';


import React from 'react';
import ReactDOM from 'react-dom';
import Moment from 'moment';

import _ from 'lodash';

var PRODUCTS = [
  { id:1, date: '01/01/2016', deliveryCountry: 'Germany', manufacturer: 'The Hipster Jeans', gender: 'F', size: '16', colour: 'Dark Blue', style: 'relaxed', count: '3'  },
  { id:2, date: '01/01/2016', deliveryCountry: 'United Kingdom', manufacturer: 'Denzil Jeans', gender: 'M', size: '32/32', colour: 'Light Blue', style: 'skinny', count: '7'  },
  { id:3, date: '02/01/2016', deliveryCountry: 'France', manufacturer: 'The Hipster Jeans', gender: 'M', size: '28/30', colour: 'Red', style: 'skinny', count: '6'  },
  { id:4, date: '02/01/2016', deliveryCountry: 'Austria', manufacturer: 'Wrangled Jeans', gender: 'F', size: '16', colour: 'Yellow', style: 'boot cut', count: '1'  }
];



//Convert sizes and dates
for (var i = 0; i < PRODUCTS.length; i++) {
	var size = PRODUCTS[i].size.split('/');
	var secSize = (size[1]) ? size[1] : size[0];

	PRODUCTS[i].sizeArea = size[0]*secSize;
	PRODUCTS[i].dateUnix = Moment(PRODUCTS[i].date, "DD-MM-YYYY").unix();
}

class ProductRow extends React.Component {
  render() {
    return (
      <tr>
        <td>{this.props.product.date}</td>
        <td>{this.props.product.deliveryCountry}</td>
        <td>{this.props.product.manufacturer}</td>
        <td>{this.props.product.gender}</td>
        <td>{this.props.product.size}</td>
        <td>{this.props.product.colour}</td>
        <td>{this.props.product.style}</td>
        <td>{this.props.product.count}</td>
      </tr>
    );
  }
}

class ProductTable extends React.Component {
  	constructor(props) {
	    super(props);
	    
	    this.state = {
	    	date: new Date(),
	    	products: this.props.products
	    };
  	}

	sortBy(options) {
		switch(options){
			case 'selling-manufacturers-by-gender-country':
			PRODUCTS = _.sortBy(PRODUCTS, ["manufacturer", "count", "country"]);
			break;

			case 'selling-sizes-by-gender':
			PRODUCTS = _.sortBy(PRODUCTS, ["sizeArea", "count", "gender"]);
			break;

			case 'selling-month-by-country':
			PRODUCTS = _.sortBy(PRODUCTS, ["dateUnix", "count", "country"]);
			break;
		}

	 	this.setState({
	  		date: new Date(),
	  		products: PRODUCTS
		});
 	}

  	render() {
	    var rows = [];
	    this.state.products.forEach(function(product) {
	      	rows.push(<ProductRow product={product} key={product.id} />);
	    });

	    return (
	    	<div>
	    	 	<button className='ui button mini' id='btn' onClick={this.sortBy.bind(this, 'selling-manufacturers-by-gender-country')}>The top selling manufacturers by gender and country</button>
	    	 	<button className='ui button mini' onClick={this.sortBy.bind(this, 'selling-sizes-by-gender')}>The top selling sizes by country</button>
	    	 	<button className='ui button mini' onClick={this.sortBy.bind(this, 'selling-month-by-country')}>The top selling months globally and by country</button>
		      	<table className='ui striped table'>
			        <thead>
			          <tr>
			            <th>Date</th>
			            <th>Delivery Country</th>
			            <th>Manufacturer</th>
			            <th>Gender</th>
			            <th>Size</th>
			            <th>Colour</th>
			            <th>Style</th>
			            <th>Count</th>
			          </tr>
			        </thead>
			        <tbody>{rows}</tbody>
		      	</table>
    	 	</div>
	    );
  	}
}

class Page extends React.Component {
 	render() {
	 	return (
	      <div>
	    	<ProductTable products={PRODUCTS} />
	      </div>
	    );
 	}
}

ReactDOM.render(
	<Page />,
  	document.getElementById('root')
);